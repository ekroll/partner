# Partner
###### About
Partner is a project circling speech recognition that is made using Python3. Currently the program repeats to you what it heard.

### Stuff you need
###### Virtualenvironment (To isolate the project from system/global packages, and easy installation)
```
python3 -m pip install virtualenv
cd wherever-partner-is-cloned/
virtualenv venv
source venv/bin/activate
python3 -m pip install -r requirements.txt
```

### If it doesnt work... install things manually
But keep the virtual environment to avoid bugs...
###### PyAudio (For live recording)
```
sudo apt install portaudio19-dev
python3 -m pip install pyaudio
```

###### pocketsphinx (Offline speech recognition)
```
sudo apt-get install -y python python-dev python-pip build-essential swig libpulse-dev
python3 -m pip install pocketsphinx
```

###### SpeechRecognition (Makes pocketsphinx easier, has support for other packages too)
```
python3 -m pip install SpeechRecognition
```

###### pyttsx3 (Offline text to speech)
```
sudo apt install espeak libespeak1
pip install git+git://github.com/nateshmbhat/pyttsx3.git
```

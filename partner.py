import pyttsx3
import pocketsphinx
import speech_recognition as sr

select_microphone = False

recognizer = sr.Recognizer()
engine = pyttsx3.init()

engine.setProperty('voice', 16)
engine.setProperty("rate", 175)
engine.setProperty("volume", 0.25)


#My handy functions: text-to-speech...
def tts(engine, text_to_convert_to_speech):
    print(text_to_convert_to_speech)
    engine.say(text_to_convert_to_speech)
    engine.runAndWait()

#And speech-to-text
def stt(recognizer, mic):
    with mic as source:
        tts(engine, "Calibrating my ears...")
        recognizer.adjust_for_ambient_noise(source)
        tts(engine, "Talk partner...")
        audio = recognizer.listen(source)
    try:
        tts(engine, "Trying to understand...")
        recognized_text = recognizer.recognize_sphinx(audio)
    except:
        recognized_text = "something unclear, talk clearer"
    return recognized_text;



#Startup
tts(engine, "Hello partner, piss off...")

if select_microphone:
    for microphone in sr.Microphone.list_microphone_names():
        print(microphone)
        tts(engine, "What microphone do you want to use? Please enter device index")
        mic_index = int(input("Enter device index: "))
        mic = sr.Microphone(device_index = mic_index)
else:
    mic = sr.Microphone()



#Loop
while True:
    the_stuff_you_said = stt(recognizer, mic)
    tts(engine, "I heard " + the_stuff_you_said)
